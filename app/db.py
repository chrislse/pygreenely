"""
DB utility file. The purpose of this file is to provide utility methods to access sqlite3 datastore
without tedious setup.

It can be ported into Orator records in the future.

Current implementation has a limitation of 1 connection per request, which needs to be improved.
"""
import sqlite3

from flask_jwt import current_identity

from user import User

def initialize_cursor():
    db = sqlite3.connect('greenely.db')
    db.text_factory = str
    return db.cursor()

def get_all_users():
    cursor = initialize_cursor()
    cursor.execute('''SELECT * FROM users''')
    all_rows = cursor.fetchall()
    users = []

    for row in all_rows:
        # row[0] returns the first column in the query (name), row[1] returns email column.
        users.append(User(row[0], row[4], row[5], row[6])) # ignore created/updated/deleted at

    return users

def get_min_max_months():
    cursor = initialize_cursor()
    # get month limits
    cursor.execute('''SELECT * FROM months where user_id = ? order by temperature DESC limit 1''',  (current_identity.id,))
    max_month = cursor.fetchone()
    cursor.execute('''SELECT * FROM months where user_id = ? order by temperature ASC limit 1''',  (current_identity.id,))
    min_month = cursor.fetchone()

    return {'temperature': {'minimum': min_month[4], 'maximum': max_month[4]},
     'consumption': {'minimum': min_month[3], 'maximum': max_month[3]},
     'timestamp': {'minimum': date_of(min_month[2]), 'maximum': date_of(max_month[2])}
    }

def get_min_max_days():
    cursor = initialize_cursor()
    cursor.execute('''SELECT * FROM days where user_id = ? order by temperature DESC limit 1''',  (current_identity.id,))
    max_day = cursor.fetchone()
    cursor.execute('''SELECT * FROM days where user_id = ? order by temperature ASC limit 1''',  (current_identity.id,))
    min_day = cursor.fetchone()

    return {'temperature': {'minimum': min_day[4], 'maximum': max_day[4]},
     'consumption': {'minimum': min_day[3], 'maximum': max_day[3]},
     'timestamp': {'minimum': date_of(min_day[2]), 'maximum': date_of(max_day[2])}
    }

def get_data(start, count, resolution):
    db = sqlite3.connect('greenely.db')
    db.text_factory = str
    cursor = db.cursor()

    if resolution == 'D':
       cursor.execute('''SELECT * FROM days where user_id = ? and timestamp >= ? order by timestamp ASC limit ?''',  (current_identity.id, start, count))
    elif resolution == 'M':
       cursor.execute('''SELECT * FROM months where user_id = ? and timestamp >= ? order by timestamp ASC limit ?''',  (current_identity.id, start, count))

    fetched_data = []
    rows = cursor.fetchall()
    for row in rows:
        fetched_data.append([date_of(row[2]), row[3], row[4]]) # ignore created/updated/deleted at

    return fetched_data

# helper methods to extract date out of timestamp
def date_of(timestamp):
    return (timestamp.split())[0]
