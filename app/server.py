"""
http server file. It provides all endpoints implementation as well as auth methods.

The authentication is implemented by using jwt.
"""
import datetime

from flask import Flask, jsonify, request, abort
from flask_jwt import JWT, jwt_required, current_identity
from werkzeug.security import safe_str_cmp
import sqlite3

from jwt_support import authenticate, identity
from db import get_min_max_months, get_min_max_days, get_data
from user import User

app = Flask(__name__)

@app.route('/api/v1/limits')
@jwt_required()
def limits():
    limits_response = {
            'limits': { 'days': get_min_max_months(), 'months': get_min_max_days()}
            }

    return jsonify(limits_response)


@app.route('/api/v1/data')
@jwt_required()
def data():
    start = request.args.get('start')
    count = request.args.get('count')
    resolution = request.args.get('resolution')

    if validate_start(start) and validate_count(count) and validate_resolution(resolution):
        data_response = {
                'data': get_data(start, count, resolution)
                }
        return jsonify(data_response)
    else:
        return abort(400, 'ResMsgInvalidDataParameters')

def validate_start(start):
    try:
        datetime.datetime.strptime(start, '%Y-%m-%d')
        return True
    except ValueError:
        return False

def validate_count(count):
    return count > 0

def validate_resolution(resolution):
    return resolution == 'D' or resolution == 'M'

with open('keys/app.rsa', 'r') as key_file:
        rsa_key=key_file.read()

app.debug = True
app.config['SECRET_KEY'] = rsa_key
app.config['JWT_AUTH_URL_RULE'] = '/api/auth/login'

jwt = JWT(app, authenticate, identity)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
