"""
user file contains class to represent user model and its utility methods
"""
import scrypt

class User(object):
    def __init__(
            self, id,
            username, password_salt,
            password_hash):

        self.id = id
        self.username = username
        self.password_salt = password_salt
        self.password_hash = password_hash

    def __str__(self):
        return "User(id='%s')" % self.id

    def generate_hash(self, password):
        return scrypt.hash(password.encode('utf-8'), self.password_salt)
