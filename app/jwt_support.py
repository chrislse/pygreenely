"""
jwt_support file to implement authentication methods when user login, which is used by jwt module
"""

from flask_jwt import JWT, jwt_required, current_identity
from werkzeug.security import safe_str_cmp
from db import get_all_users

users = get_all_users()

username_table = {u.username: u for u in users}
userid_table = {u.id: u for u in users}

def authenticate(username, password):
    user = username_table.get(username, None)
    if user and safe_str_cmp(user.password_hash, user.generate_hash(password)):
        return user
    else:
        return None

def identity(payload):
    user_id = payload['identity']
    return userid_table.get(user_id, None)

