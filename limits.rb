require 'csv'
require 'httparty'
require 'json'


class Limits
  include HTTParty

  base_uri 'localhost:5000'

  def initialize()
    @token = ''
  end

  def login(username, password)
    response = self.class.post('/api/auth/login', body: "#{{username: username, password: password}.to_json}", headers: {'Content-Type' => 'application/json'})
    puts response
    @token = response["access_token"]
    response
  end

  def get_limits()
    self.class.get('/api/v1/limits', headers: {'Authorization' => "JWT #{@token}"} )
  end
end

limit_endpoint = Limits.new
limit_endpoint.login("snowballshiloh", "Th1sEspassword")
puts limit_endpoint.get_limits()

