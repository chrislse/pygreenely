.PHONY: docker run

docker:
	docker build . -t pygreenely

run: docker
	docker run -p 5000:5000 pygreenely
