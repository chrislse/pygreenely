a python version of greenely exercise, to show that I am confortable across languages.

the project follows this style guide, https://www.python.org/dev/peps/pep-0008/

# How to start the project

## Local executable requirements
* python2
* ruby (for test)

### Setup
* pip install -r requirements.txt

### Run
* python2 app/server.py

## Docker executable requirements
* docker
* make

### Run
* make run


### Test
* gem install httparty or sudo gem install httparty
* ruby limits.rb
* ruby data.rb

## How test script works
The ruby scripts try to login as a normal user and then do http request to the endpoints, with given auth token.

A list of usernames can be found in usernames.txt. All users share the same passowrd:
```
Th1sEspassword
```


# Model Diagram


      +--------------------+       +---------------------+
      | users              |       | days                |
      |                    |       +---------------------+
      +--------------------+ ref   | day_id     pk       |
      | id    pk           <-+-----+ user_id             |
      | created_at         | |     | timestamp           |
      | updated_at         | |     | consumption         |
      | deleted_at         | |     | temperature         |
      | username           | |     |                     |
      | password_salt      | |     +---------------------+
      | password_hash      | |
      |                    | |     +---------------------+
      |                    | |     | months              |
      +--------------------+ |     +---------------------+
                             |     | month_id    pk      |
                             +-----+ user_id             |
                                   | timestamp           |
                             ref   | consumption         |
                                   | temperature         |
                                   |                     |
                                   +---------------------+

# Todo
- [x] complete the flow
- [x] dockerize the app
- [x] style and format the code
- [x] validate params when fetching data
- [x] read public key from rsa file
- [ ] use class view on resources
- [ ] use Orator to access database
- [ ] add a pipeline to push to aws

