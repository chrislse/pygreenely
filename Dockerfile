FROM python:2.7

WORKDIR /usr/src/pygreenely

COPY . .

RUN apt update && \
      apt install sqlite3 &&\
      pip install --no-cache-dir -r requirements.txt &&\
      ./setup.sh


CMD ["python", "./app/server.py"]
