#!/bin/sh

# prepare db with seeds
rm -rf greenely.db
cat db/table_creations.sql | sqlite3 greenely.db
cat db/users.sql | sqlite3 greenely.db
cat db/months.sql | sqlite3 greenely.db
cat db/days.sql | sqlite3 greenely.db

# prepare server side public/private key for jwt
openssl genrsa -out keys/app.rsa 1024
openssl rsa -in keys/app.rsa -pubout > keys/app.rsa.pub

