CREATE TABLE IF NOT EXISTS users (
	id integer primary key autoincrement,
	created_at datetime,
	updated_at datetime,
	deleted_at datetime,
	username varchar(255),
	password_salt varchar(255),
	password_hash varchar(255)
	);

CREATE INDEX IF NOT EXISTS idx_users_deleted_at ON users(deleted_at);

CREATE TABLE IF NOT EXISTS months (
        month_id INTEGER PRIMARY KEY AUTOINCREMENT,
	user_id INTEGER NOT NULL,
	timestamp TEXT NOT NULL,
	consumption INTEGER NOT NULL,
	temperature INTEGER NOT NULL,
	   FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE IF NOT EXISTS days (
	day_id INTEGER PRIMARY KEY AUTOINCREMENT,
	user_id INTEGER NOT NULL,
	timestamp TEXT NOT NULL,
	consumption INTEGER NOT NULL,
	temperature INTEGER NOT NULL,
	   FOREIGN KEY (user_id) REFERENCES user(id)
);
